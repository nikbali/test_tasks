import java.util.*;
/*
Задача 1.
Есть массив чисел от 1 до 100000000000.
Массив отсортирован(1,2,3 ....99999999999,100000000000 ), но в нем  одна цифра задвоилась(Например, [1,2,3,4,4,6,7 ...]).
Надо написать метод который найдет отсутствующее число за минимальное время.
 */
public class Task1 {
    public static void main(String[] args) {
        Date start, end;
        //массив на 100000000000 элементов JVM не дает создать, т.к будет переполнение в куче
        //максимальный размер ограничевается Integer.MAX_VALUE - 8
        ArrayList<Integer> arr = genArray(10000000);
        start = new Date();
        int s = searchDublicate(arr);
        end = new Date();
        System.out.println("Отсутствующее число: " + s);
        System.out.println("Время поиска: " + (end.getTime() - start.getTime()) + " мc");

    }
    //ф-ия генерерует массив отсортированных чисел с одним задвоившемся числом
    public static ArrayList<Integer> genArray(int len) {
        final Random random = new Random();
        int rand_index = random.nextInt(len);
        ArrayList<Integer> arr = new ArrayList<Integer>();
        for (int i = 1; i <= len; i++) {
            arr.add(i);
        }
        arr.set(rand_index - 1, rand_index - 1);
        System.out.println("Сгенерирован массив на " + len + " элементов");
        return arr;
    }

    //ф-ия ишет отсутствующее число
    public static int searchDublicate(ArrayList<Integer> arr) {
        Set<Integer> foundInt = new HashSet<>();
        for (int ch : arr) {
            if (foundInt.contains(ch)) {
                return ch + 1;
            } else {
                foundInt.add(ch);
            }
        }
        return 0;
    }
}
