import java.io.*;
/*
* Задача 2. Есть файл с текстом, надо написать метод который выведет каждое третье слово из текста.
*/
public class Task2 {
    public static void main(String[] args) {
        System.out.println("Введите путь к файлу: ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            FileInputStream fileIn = new FileInputStream(reader.readLine());
            printThreeWord(fileIn);
        } catch (IOException ex) {
            System.out.println("Файл не существует!");
        }
    }
    //метод выводит в консоль каждое третье слово из InputStream
    public static void printThreeWord(InputStream in) throws IOException {
        try {
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader reader = new BufferedReader(isr);
            int size_word = 0; //храним номер слова (по порядку в файле)
            while (true) {
                String line = reader.readLine();
                if (line == null) break;
                String[] words = line.split(" ");
                for (String word : words) {
                    size_word++;
                    if (size_word % 3 == 0) System.out.println(word);
                }
            }
            reader.close();
            isr.close();
        } catch (IOException e) {
            System.out.println("Ошибка чтения файла!");
        }
    }
}
